<?php

namespace Creational\Builder\Packing;

use Creational\Builder\Interfaces\Packing;

/**
 * Class Bottle
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Packing As a common definition.
 */
class Bottle implements Packing {
	
	/**
	 * Get the packing.
	 * 
	 * @return	String The packing.
	 */
	public function pack() { return "Bottle"; }
	
}
