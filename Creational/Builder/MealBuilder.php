<?php

namespace Creational\Builder;

use Creational\Builder\Drinks\Cold\Coke;
use Creational\Builder\Drinks\Cold\Pepsi;
use Creational\Builder\Food\Burgers\Veg;
use Creational\Builder\Food\Burgers\Chicken;

/**
 * Class MealBuilder
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Coke As part of the meal drinks selection.
 * @uses	Pepsi As part of the meal drinks selection.
 * @uses	Veg As part of the meal burger selection.
 * @uses	Chicken As part of the meal burger selection.
 */
class MealBuilder {

	/**
	 * MealBuilder constructor.
	 */
	public function __construct() {
		;
	}
	
	/**
	 * Build a veg burger meal.
	 *
	 * @return	Meal The veg burger meal.
	 */
	public function prepareVegMeal() {
		// create the meal
		$meal = new Meal();
		// add specific items to the meal for the veg burger meal
		$meal->addItem(new Veg());
		$meal->addItem(new Coke());
		// return the build meal object
		return $meal;
	}
	
	/**
	 * Build a chicken burger meal.
	 *
	 * @return	Meal The chicken burger meal.
	 */
	public function prepareChickenMeal() {
		// create the meal
		$meal = new Meal();
		// add specific items to the meal for the chicken burger meal
		$meal->addItem(new Chicken());
		$meal->addItem(new Pepsi());		
		// return the build meal object
		return $meal;
	}
	
}
