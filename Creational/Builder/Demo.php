<?php

namespace Creational\Builder;

use Common\Demo\APatternDemo;

/**
 * Class Demo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	APatternDemo As a design pattern demo base.
 */
class Demo extends APatternDemo {
	
	/**
	 * Demo constructor.
	 */
	public function __construct() {
		parent::__construct("Builder", "Creational");
	}
	
	/**
	 * Run the Builder Pattern Demonstraction.
	 */
	public function run() {
		echo "\nRunning {$this->pattern_type} type {$this->pattern_name} design pattern demonstration\n";
		// get the builder object
		$mealBuilder = new MealBuilder();
		echo "Building Veg Meal\n";
		// build a veg burger meal object
		$vegMeal = $mealBuilder->prepareVegMeal();
		$vegMeal->showItems();
		echo "Total : " . $vegMeal->getCost() . "\n\n";
		echo "Building Chicken Meal\n";
		// build a chicken burger meal object
		$chickenMeal = $mealBuilder->prepareChickenMeal();
		$chickenMeal->showItems();
		echo "Total : " . $chickenMeal->getCost() . "\n\n";
		
	}
	
}
