<?php

namespace Creational\Builder\Drinks\Cold;

use Creational\Builder\Drinks\Cold;

/**
 * Class Coke
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Cold As a base type for cold drinks.
 */
class Coke extends Cold {

	/**
	 * Coke constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Get the items name.
	 * 
	 * @return	String The item name.
	 */
	public function name() { return "Coke"; }
	
	/**
	 * Get the items price.
	 * 
	 * @return	Float The item price.
	 */
	public function price() { return 1.50; }
	
}
