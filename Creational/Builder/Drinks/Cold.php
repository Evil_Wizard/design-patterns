<?php

namespace Creational\Builder\Drinks;

use Creational\Builder\Interfaces\Item;
use Creational\Builder\Interfaces\Packing;
use Creational\Builder\Packing\Bottle;

/**
 * Abstract Class Cold
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Item As a common definition.
 * @uses	Packing To define the burgers wrapper packing.
 * @uses	Bottle As the burgers packing.
 */
abstract class Cold implements Item {

	/**
	 * Cold constructor.
	 */
	protected function __construct() {
		;
	}
	
	/**
	 * Get the cold drinks packing detail.
	 * 
	 * @return	Packing The cold drinks packing detail.
	 */
	public function packing() { return new Bottle(); }

}
