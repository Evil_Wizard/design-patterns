<?php

namespace Creational\Builder\Interfaces;

/**
 * Interface Item
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Packing To define the items wrapper packing.
 */
interface Item {
	
	/**
	 * Get the items name.
	 * 
	 * @return	String The item name.
	 */
	public function name();
	
	/**
	 * Get the items price.
	 * 
	 * @return	Float The item price.
	 */
	public function price();
	
	/**
	 * Get the items packing detail.
	 * 
	 * @return	Packing The items packing detail.
	 */
	public function packing();
	
}
