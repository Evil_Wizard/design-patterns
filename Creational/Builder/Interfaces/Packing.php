<?php

namespace Creational\Builder\Interfaces;

/**
 * Interface Packing
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
interface Packing {
	
	/**
	 * Get the packing.
	 * 
	 * @return	String The packing.
	 */
	public function pack();
	
}
