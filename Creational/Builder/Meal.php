<?php

namespace Creational\Builder;

use Creational\Builder\Interfaces\Item;

/**
 * Class Meal
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Item To define meal items.
 */
class Meal {
	
	/*
	 * @var	Array List of meal items.
	 */
	protected $items = [];

	/**
	 * Meal constructor.
	 */
	public function __construct() {
		;
	}
	
	/**
	 * Add an item to the meal.
	 *
	 * @param	Item $item The item to add to the meal.
	 */
	public function addItem(Item $item) { $this->items[] = $item; }
	
	/**
	 * Get the total cost of the items in the meal.
	 *
	 * @return	Float The total cost of the items in the meal.
	 */
	public function getCost() {
		$cost = 0;
		// loop through the items in the meal & totalise the cost price
		foreach($this->items as $item) {
			$cost += $item->price();
		}
		return $cost;
	}
	
	/**
	 * Display a list of item details for all items in the meal.
	 */
	public function showItems() {
		// loop through the meal items & show details
		foreach($this->items as $item) {
			echo "Item : " . $item->name() . "\n";
			echo "Packing : " . $item->packing()->pack() . "\n";
			echo "Price : " . $item->price() . "\n\n";
		}
	}
}
