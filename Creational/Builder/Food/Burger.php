<?php

namespace Creational\Builder\Food;

use Creational\Builder\Interfaces\Item;
use Creational\Builder\Interfaces\Packing;
use Creational\Builder\Packing\Wrapper;

/**
 * Abstract Class Burger
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Item As a common definition.
 * @uses	Packing To define the burgers wrapper packing.
 * @uses	Wrapper As the burgers packing.
 */
abstract class Burger implements Item {

	/**
	 * Burger constructor.
	 */
	protected function __construct() {
		;
	}
	
	/**
	 * Get the burgers packing detail.
	 * 
	 * @return	Packing The burgers packing detail.
	 */
	public function packing() { return new Wrapper(); }

}
