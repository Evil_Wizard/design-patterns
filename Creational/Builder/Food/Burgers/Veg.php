<?php

namespace Creational\Builder\Food\Burgers;

use Creational\Builder\Food\Burger;

/**
 * Class Veg
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Burger As a base type for burgers.
 */
class Veg extends Burger {

	/**
	 * Veg constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * Get the items name.
	 * 
	 * @return	String The item name.
	 */
	public function name() { return "Veg Burger"; }
	
	/**
	 * Get the items price.
	 * 
	 * @return	Float The item price.
	 */
	public function price() { return 1.29; }
	
}
