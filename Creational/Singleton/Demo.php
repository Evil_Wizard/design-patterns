<?php

namespace Creational\Singleton;

use Common\Demo\APatternDemo;

/**
 * Class Demo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	APatternDemo As a design pattern demo base.
 */
class Demo extends APatternDemo {
	
	/**
	 * Demo constructor.
	 */
	public function __construct() {
		parent::__construct("Singleton", "Creational");
	}
	
	/**
	 * Run the Singleton Pattern Demonstraction.
	 */
	public function run() {
		echo "\nRunning {$this->pattern_type} type {$this->pattern_name} design pattern demonstration\n";
		// get a number of singleton instances
		$singleton = Singleton::getInstance();
		$singleton->incrementValue();
		Singleton::getInstance()->showValue();
		$singleton1 = Singleton::getInstance();
		$singleton->incrementValue();
		$singleton1->showValue();
		$singleton1->incrementValue();
		$singleton1->showValue();
		Singleton::getInstance()->decrementValue();
		$singleton3 = Singleton::getInstance();
		$singleton3->showValue();
	}
	
}
