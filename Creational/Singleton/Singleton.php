<?php

namespace Creational\Singleton;

/**
 * Instance Class Singleton
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
class Singleton {
	
	/*
	 * @var Singleton The single instance of the Singleton.
	 */
	protected static $ourInstance = NULL;
	
	/*
	 * @var Integer An object instance value.
	 */
	protected $value = 0;

	/**
	 * Singleton constructor.
	 */
	protected function __construct() {
		echo "New SingletonPattern created\n";
		$this->showValue();
	}

	/**
	 * Singleton destructor.
	 */
	public function __destruct() {
		static::$ourInstance = NULL;
	}

	/**
	 * This object should be treated as a singleton instance.
	 * 
	 * @return	Singleton The single instance of the Singleton.
	 */
	public static function &getInstance() {
		if (is_null(static::$ourInstance)) {
			// the instance needs creating
			static::$ourInstance = new static();
		}
		echo "Got SingletonPattern instance\n";
		// return the instance reference
		return static::$ourInstance;
	}
	
	/**
	 * Increase the object instance value by 1.
	 */
	public function incrementValue() { $this->value++; }
	
	/**
	 * Decrease the object instance value by 1.
	 */
	public function decrementValue() { $this->value--; }
	
	/**
	 * Show the object instance value.
	 */
	public function showValue() { 
		echo "Object instance value [{$this->value}]\n";
	}

}
