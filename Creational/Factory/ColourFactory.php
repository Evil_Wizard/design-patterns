<?php

namespace Creational\Factory;

use Creational\AbstractFactory\AbstractFactory;
use Creational\Factory\Colours\IColour;
use ReflectionClass;
use ReflectionException;
use Exception;

/**
 * Class ColourFactory
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	AbstractFactory As a base to also demonstrate the Abstract Factory pattern.
 * @uses	IColour To define the factory colour product.
 * @uses	ReflectionClass To get the factory colour product.
 * @uses	ReflectionException Catches named exception.
 * @uses	Exception For error and exception handling.
 */
class ColourFactory extends AbstractFactory {
	
	/*
	 * @var	Array List of colour types and the object name.
	 */
	protected $colours = [
		"RED"		=> "Red",
		"GREEN"		=> "Green",
		"BLUE"		=> "Blue",
	];
	
	/**
	 * ShapeFactory constructor.
	 */
	public function __construct() {
		parent::__construct();
		echo "New ColourFactory created\n";
	}
	
	/**
	 * Get a specified factory colour product.
	 * 
	 * @param	String $colour The factory colour type.
	 * @return	Colour The required factory colour product.
	 * @throws	Exception If the factory colour product is unknown.
	 */
	public function getColour($colour) { 
		$colour_type = strtoupper($colour);
		// determine what factory colour product has been requested
		if(!array_key_exists($colour_type, $this->colours)) {
		// unknown design pattern demo requested
			throw new Exception("Unknown factory colour [{$colour}]");
		}
		// construct the namespace for the factory output object
		$colour_class_ns = "\Creational\Factory\Colours\\{$this->colours[$colour_type]}";
		try {
			// try to get the factory output object
			$colour_reflection = new ReflectionClass($colour_class_ns);
		} catch (ReflectionException $ex) {
		// factory output object didn't exist
			throw new Exception("ERROR [{$colour_class_ns}] can not be found", $ex->getCode(), $ex);
		}
		if(!$colour_reflection->isSubclassOf(IColour::class)) {
		// factory output object is not of a recognised type
			throw new Exception("ERROR [{$colour_class_ns}] is not a recognised factory colour");							
		}
		// return the factory colour
		return $colour_reflection->newInstance();
	}
	
	/**
	 * Method required to fulfil the abstract factory base definition but is unused
	 * in the specific factory.
	 * 
	 * @param	String $shape The factory shape type.
	 * @return	Shape The required factory shape product.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	public function getShape($shape) { throw new Exception("Unsupported Factory Action"); }
}
