<?php

namespace Creational\Factory;

use Common\Demo\APatternDemo;
use Common\ShapeTypes;
use Exception;

/**
 * Class Demo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	APatternDemo As a design pattern demo base.
 * @uses	ShapeTypes For the factory shape product.
 * @uses	Exception For error and exception handling.
 */
class Demo extends APatternDemo {
	
	/**
	 * Demo constructor.
	 *
	 * @param	String $pattern_name Optional overriding design type pattern name.
	 * @param	String $pattern_type Optional overriding design type group the pattern falls into.
	 */
	public function __construct($pattern_name=NULL, $pattern_type=NULL) {
		$name = (is_null($pattern_name)) ? "Factory" : $pattern_name;
		$type = (is_null($pattern_name)) ? "Creational" : $pattern_type;
		parent::__construct($name, $type);
	}
	
	/**
	 * Run the Factory Pattern Demonstraction.
	 */
	public function run() {
		// demo the static:: implementation of the factory pattern
		echo "\nRunning {$this->pattern_type} type Static:: implementation {$this->pattern_name} design pattern demonstration\n";
		$this->runStaticShapeFactory();
		// demo the new() implementation of the factory pattern
		echo "\nRunning {$this->pattern_type} type New() implementation {$this->pattern_name} design pattern demonstration\n";
		$colours_factory = new ColourFactory();
		$this->runNewColourFactory($colours_factory);
	}
	
	/**
	 * Run the Factory Pattern Demonstraction as a new instance version using the ColourFactory.
	 *
	 * @param	ColourFactory $colours_factory The colour factory to use in the demonstration.
	 */
	protected function runNewColourFactory(ColourFactory $colours_factory) {
		try {
			// get the colours from the factory
			$red = $colours_factory->getColour("red");
			$green = $colours_factory->getColour("green");
			$blue = $colours_factory->getColour("blue");
			// fill the colour
			$red->fill();
			$green->fill();
			$blue->fill();
		} catch (Exception $ex) {
		// something is not right
			echo "Unexpected Error\n";
			echo $ex->getMessage() . "\n";
		}
		try {
			// try to get an unknown colour to force the factory to produce an error
			$unknown_colour = $colours_factory->getColour("-1");
			$unknown_colour->fill();
		} catch (Exception $ex) {
		// this is the expected result
			echo "Expected Error\n";
			echo $ex->getMessage() . "\n";
		}
	}
	
	/**
	 * Run the Factory Pattern Demonstraction as a new instance version using the ShapeFactory.
	 *
	 * @param	ShapeFactory $shapes_factory The shape factory to use in the demonstration.
	 */
	protected function runNewShapeFactory(ShapeFactory $shapes_factory) {
		try {
			// get the shapes from the factory
			$circle = $shapes_factory->getShape(ShapeTypes::CIRCLE);
			$square = $shapes_factory->getShape(ShapeTypes::SQUARE);
			$rectangle = $shapes_factory->getShape(ShapeTypes::RECTANGLE);
			// draw the shapes
			$circle->draw();
			$square->draw();
			$rectangle->draw();
		} catch (Exception $ex) {
		// something is not right
			echo "Unexpected Error\n";
			echo $ex->getMessage() . "\n";
		}
		try {
			// try to get an unknown shape to force the factory to produce an error
			$unknown_shape = $shapes_factory->getShape("-1");
			$unknown_shape->draw();
		} catch (Exception $ex) {
		// this is the expected result
			echo "Expected Error\n";
			echo $ex->getMessage() . "\n";
		}
	}
	
	/**
	 * Run the Factory Pattern Demonstraction as a static instance version using the ShapeFactory.
	 */
	protected function runStaticShapeFactory() {
		try {
			// get the shapes from the factory
			$circle = StaticShapeFactory::getShape(ShapeTypes::CIRCLE);
			$square = StaticShapeFactory::getShape(ShapeTypes::SQUARE);
			$rectangle = StaticShapeFactory::getShape(ShapeTypes::RECTANGLE);
			// draw the shapes
			$circle->draw();
			$square->draw();
			$rectangle->draw();
		} catch (Exception $ex) {
		// something is not right
			echo "Unexpected Error\n";
			echo $ex->getMessage() . "\n";
		}
		try {
			// try to get an unknown shape to force the factory to produce an error
			$unknown_shape = StaticShapeFactory::getShape("-1");
			$unknown_shape->draw();
		} catch (Exception $ex) {
		// this is the expected result
			echo "Expected Error\n";
			echo $ex->getMessage() . "\n";
		}		
	}
	
}
