<?php

namespace Creational\Factory\Colours;

/**
 * Class Red
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	IColour As a common definition.
 */
class Red implements IColour {
	
	/**
	 * Red constructor.
	 */
	public function __construct() {
		echo "New Colour Red created\n";
	}
	
	/**
	 * Fill with the specific colour.
	 */
	public function fill() {
		// the specific logic to fill the colour
		echo "Filling in red\n";		
	}
}
