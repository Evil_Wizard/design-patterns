<?php

namespace Creational\Factory\Colours;

/**
 * Interface IColour
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
interface IColour {
	
	/**
	 * Fill with the specific colour.
	 */
	public function fill();
}
