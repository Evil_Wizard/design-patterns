<?php

namespace Creational\Factory\Colours;

/**
 * Class Green
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	IColour As a common definition.
 */
class Green implements IColour {
	
	/**
	 * Green constructor.
	 */
	public function __construct() {
		echo "New Colour Green created\n";
	}
	
	/**
	 * Fill with the specific colour.
	 */
	public function fill() {
		// the specific logic to fill the colour
		echo "Filling in green\n";		
	}
}
