#  Factory Pattern
----

Factory pattern type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.

In Factory pattern, we create object without exposing the creation logic to the client and refer to newly created object using a common interface.

## Implementation
----

Included is a StaticShapeFactory implementation of the factory pattern to demonstration a static instance variation of the pattern.
The ShapeFactory and ColourFactory need to be instantiated via the _new_ keyword and are also used in the [Abstract Factory](../AbstractFactory/abstractfactory.md) pattern.
The example uses a defined list of Colours or ShapeTypes using ReflectionsClass to include and instantiate the requested Colour or Shape respectively.
With the FQCN being constructed as a String for the specific Shape and Colour the objects can be grouped into namespaces.

## Challenge
----
