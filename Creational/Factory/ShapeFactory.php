<?php

namespace Creational\Factory;

use Creational\AbstractFactory\AbstractFactory;
use Common\ShapeTypes;
use Common\Shape;
use ReflectionClass;
use ReflectionException;
use Exception;

/**
 * Class ShapeFactory
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	AbstractFactory As a base to also demonstrate the Abstract Factory pattern.
 * @uses	ShapeTypes To determine the factory shape product.
 * @uses	Shape To define the factory shape product.
 * @uses	ReflectionClass To get the factory shape product.
 * @uses	ReflectionException Catches named exception.
 * @uses	Exception For error and exception handling.
 */
class ShapeFactory extends AbstractFactory {
	
	/*
	 * @var	Array List of shape types and the object name.
	 */
	protected $shapes = [
		ShapeTypes::CIRCLE		=> "Circle",
		ShapeTypes::SQUARE		=> "Square",
		ShapeTypes::RECTANGLE	=> "Rectangle",
	];
	
	/**
	 * ShapeFactory constructor.
	 */
	public function __construct() {
		parent::__construct();
		echo "New ShapeFactory created\n";
	}
	
	/**
	 * Get a specified factory shape product.
	 * 
	 * @param	String $shape The factory shape type.
	 * @return	Shape The required factory shape product.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	public function getShape($shape) {
		$shape_type = strtoupper($shape);
		// determine what factory shape product has been requested
		if(!array_key_exists($shape_type, $this->shapes)) {
		// unknown design pattern demo requested
			throw new Exception("Unknown factory shape [{$shape}]");
		}
		// construct the namespace for the factory output object
		$shape_class_ns = "\Common\Shapes\\{$this->shapes[$shape_type]}";
		try {
			// try to get the factory output object
			$shape_reflection = new ReflectionClass($shape_class_ns);
		} catch (ReflectionException $ex) {
		// factory output object didn't exist
			throw new Exception("ERROR [{$shape_class_ns}] can not be found", $ex->getCode(), $ex);
		}
		if(!$shape_reflection->isSubclassOf(Shape::class)) {
		// factory output object is not of a recognised type
			throw new Exception("ERROR [{$shape_class_ns}] is not a recognised factory shape");
		}
		// return the factory shape
		return $shape_reflection->newInstance();
	}
	
	/**
	 * Method required to fulfil the abstract factory base definition but is unused
	 * in the specific factory.
	 * 
	 * @param	String $colour The factory shape type.
	 * @return	Colour The required factory shape product.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	public function getColour($colour) { throw new Exception("Unsupported Factory Action"); }
}
