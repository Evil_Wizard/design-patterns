<?php

namespace Creational\Factory;

use Common\ShapeTypes;
use Common\Shape;
use ReflectionClass;
use ReflectionException;
use Exception;

/**
 * Class StaticShapeFactory
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	ShapeTypes To determine the factory shape product.
 * @uses	Shape To define the factory shape product.
 * @uses	ReflectionClass To get the factory shape product.
 * @uses	ReflectionException Catches named exception.
 * @uses	Exception For error and exception handling.
 */
class StaticShapeFactory {
	
	/*
	 * @var	Array List of shape types and the object name.
	 */
	protected static $shapes = [
		ShapeTypes::CIRCLE		=> "Circle",
		ShapeTypes::SQUARE		=> "Square",
		ShapeTypes::RECTANGLE	=> "Rectangle",
	];
	
	/**
	 * Static function to get a specified factory shape product.
	 * 
	 * @param	String $shape The factory shape type.
	 * @return	Shape The required factory shape product.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	public static function getShape($shape) {
		$shape_type = strtoupper($shape);
		// determine what factory shape product has been requested
		if(!array_key_exists($shape_type, static::$shapes)) {
		// unknown design pattern demo requested
			throw new Exception("Unknown factory shape [{$shape}]");
		}
		// construct the namespace for the factory output object
		$shape_class_ns = "\Common\Shapes\\" . static::$shapes[$shape_type];
		try {
			// try to get the factory output object
			$shape_reflection = new ReflectionClass($shape_class_ns);
		} catch (ReflectionException $ex) {
		// factory output object didn't exist
			throw new Exception("ERROR [{$shape_class_ns}] can not be found", $ex->getCode(), $ex);
		}
		if(!$shape_reflection->isSubclassOf(Shape::class)) {
		// factory output object is not of a recognised type
			throw new Exception("ERROR [{$shape_class_ns}] is not a recognised factory shape");							
		}
		// return the factory shape
		return $shape_reflection->newInstance();
	}
}
