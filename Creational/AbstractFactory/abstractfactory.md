# Abstract Factory Pattern
----

Abstract Factory patterns work around a super-factory which creates other factories. 
This factory is also called as factory of factories. 
This type of design pattern comes under creational pattern as this pattern provides one of the best ways to create an object.
In Abstract Factory pattern an interface is responsible for creating a factory of related objects without explicitly specifying their classes.
Each generated factory can give the objects as per the Factory pattern.

## Implementation
----

Using the ColourFactory and the ShapeFactory from the [Factory](../Factory/factory.md) pattern example, the factory of factories returns the requested factory for use.
The example uses a defined list of Factories using ReflectionsClass to include and instantiate the requested Factory.
With the FQCN being defined as a String for the specific ShapeFactory and ColourFactory the factories can come from different namespaces.

## Challenge
----
