<?php

namespace Creational\AbstractFactory;

use Common\Interfaces\Shape;
use Common\Interfaces\Colour;
use Exception;

/**
 * Abstract Class AbstractFactory
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Shape To define the factory shape product.
 * @uses	Colour To define the factory colour product.
 * @uses	Exception For error and exception handling.
 */
abstract class AbstractFactory {

	/**
	 * AbstractFactory constructor.
	 */
	protected function __construct() {
		;
	}
	
	/**
	 * Get a specified factory shape product.
	 * 
	 * @param	String $shape The factory shape type.
	 * @return	Shape The required factory shape product.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	abstract public function getShape($shape);
	
	/**
	 * Get a specified factory colour product.
	 * 
	 * @param	String $colour The factory colour type.
	 * @return	Colour The required factory colour product.
	 * @throws	Exception If the factory colour product is unknown.
	 */
	abstract public function getColour($colour);

}
