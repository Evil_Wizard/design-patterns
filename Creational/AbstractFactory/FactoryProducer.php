<?php

namespace Creational\AbstractFactory;

use ReflectionClass;
use ReflectionException;
use Exception;

/**
 * Class FactoryProducer
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	ReflectionClass To get the requested factory.
 * @uses	ReflectionException Catches named exception.
 * @uses	Exception For error and exception handling.
 */
class FactoryProducer {
	
	/*
	 * @var	Array List of factory types and the object FQCN.
	 */
	protected $factories = [
		"SHAPE"		=> "\Creational\Factory\ShapeFactory",
		"COLOUR"	=> "\Creational\Factory\ColourFactory"
	];
	
	/**
	 * FactoryProducer constructor.
	 */
	public function __construct() {
		echo "New FactoryProducer created\n";
	}
	
	/**
	 * Get a specified factory.
	 * 
	 * @param	String $factory The factory type.
	 * @return	AbstractFactory The required factory.
	 * @throws	Exception If the factory colour product is unknown.
	 */
	public function getFactory($factory) { 
		$factory_type = strtoupper($factory);
		// determine what factory colour product has been requested
		if(!array_key_exists($factory_type, $this->factories)) {
		// unknown design pattern demo requested
			throw new Exception("Unknown factory [{$factory}]");
		}
		try {
			// try to get the factory output object
			$factory_reflection = new ReflectionClass($this->factories[$factory_type]);
		} catch (ReflectionException $ex) {
		// factory object didn't exist
			throw new Exception("ERROR [{$this->factories[$factory_type]}] can not be found", $ex->getCode(), $ex);
		}
		if(!$factory_reflection->isSubclassOf(AbstractFactory::class)) {
		// factory object is not of a recognised type
			throw new Exception("ERROR [{$this->factories[$factory_type]}] is not a recognised factory");							
		}
		// return the factory
		return $factory_reflection->newInstance();
	}
	
}
