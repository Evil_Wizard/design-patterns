<?php

namespace Creational\AbstractFactory;

use Creational\Factory\Demo as FactoryPatternDemo;

/**
 * Class Demo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Demo As a base to run other factory demonstrations.
 */
class Demo extends FactoryPatternDemo {
	
	/**
	 * Demo constructor.
	 */
	public function __construct() {
		parent::__construct("AbstractFactory", "Creational");
	}
	
	/**
	 * Run the Abstract Factory Pattern Demonstraction.
	 */
	public function run() {
		echo "\nRunning {$this->pattern_type} type New() implementation {$this->pattern_name} design pattern demonstration\n";
		// get the factory of factories
		$factory = new FactoryProducer();
		echo "\nUsing the ShapeFactory to demonstrate\n";
		$this->runNewShapeFactory($factory->getFactory("Shape"));
		echo "\nUsing the ColourFactory to demonstrate\n";
		$this->runNewColourFactory($factory->getFactory("colour"));
	}
	
}
