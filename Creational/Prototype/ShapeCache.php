<?php

namespace Creational\Prototype;

use Common\Shape;
use Common\Shapes\Circle;
use Common\Shapes\Square;
use Common\Shapes\Rectangle;
use Exception;

/**
 * Class ShapeCache
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Shape As a common deffinition of shapes.
 * @uses	Circle To create a new shape object to be cached.
 * @uses	Square To create a new shape object to be cached.
 * @uses	Rectangle To create a new shape object to be cached.
 * @uses	Exception For error and exception handling.
 */
class ShapeCache {
	
	/*
	 * @var	Array The associative collection of cached objects.
	 */
	private static $cache = [];
	
	/**
	 * Static method to get a cached shape object by ID.
	 *
	 * @param	String $id The shape ID.
	 * @return	Shape The shape object.
	 * @throws	Exception If the factory shape product is unknown.
	 */
	public static function getShape($id) {
		if(!array_key_exists($id, static::$cache)) {
		// the object has not been loaded & cached
			throw new Exception("Shape [{$id}] is not cached");
		}
		// return a copy of the cached shape object
		$shape = static::$cache[$id];
		return clone $shape;
	}
	
	/**
	 * Fill the cache with static example objects.
	 * 
	 * In a real world production version of the Prototype the heavy cost of producing 
	 * the new object would be performed when loading into the cache and only once
	 */
	public static function loadStaticCacheExample() {
		$circle = new Circle();
		$circle->setId("1");
		static::$cache[$circle->getId()] = $circle;
		$square = new Square();
		$square->setId("2");
		static::$cache[$square->getId()] = $square;
		$rectangle = new Rectangle();
		$rectangle->setId("3");
		static::$cache[$rectangle->getId()] = $rectangle;
	}
}
