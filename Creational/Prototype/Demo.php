<?php

namespace Creational\Prototype;

use Common\Demo\APatternDemo;

/**
 * Class Demo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	APatternDemo As a design pattern demo base.
 */
class Demo extends APatternDemo {
	
	/**
	 * Demo constructor.
	 */
	public function __construct() {
		parent::__construct("Prototype", "Creational");
	}
	
	/**
	 * Run the Prototype Pattern Demonstraction.
	 */
	public function run() {
		echo "\nRunning {$this->pattern_type} type {$this->pattern_name} design pattern demonstration\n";
		// load the prototyped shape object cache
		ShapeCache::loadStaticCacheExample();
		$clone1 = ShapeCache::getShape("1");
		echo "Shape : " . $clone1->getType() . "\n";
		$clone2 = ShapeCache::getShape("2");
		echo "Shape : " . $clone2->getType() . "\n";
		$clone3 = ShapeCache::getShape("3");
		echo "Shape : " . $clone3->getType() . "\n";
	}
	
}
