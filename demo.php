#!/usr/bin/php
<?php
// get the full path to the application folder to make sure the required files
// are found without ambiguity
define("APP_PATH", realpath("."));
spl_autoload_register("autoloader");

/**
 * Autoload namespaced resources.
 * 
 * @param	String $class_name The fully qualified class name being requested.
 * @return	Boolean TRUE if the resource was auto loaded.
 */
function autoloader($class_name) {
	// replace escaped namespace sections
	$namespaced_class = str_replace("\\", '/', $class_name);
	// get the FQCN
	$full_file_path = APP_PATH . "/{$namespaced_class}.php";
	if(file_exists($full_file_path)) {
	// require and use
		require($full_file_path);
		return TRUE;
	} else {
	// namespace file not found, look elsewhere
		return FALSE;
	}
}

/**
 * Change the colour for console output.
 * 
 * @param	String $colour The output colour to use.
 */
function clic($colour) {
	// check to see which colour code needs to be echoed
	switch ($colour) {
		case "red": 
			$code = "\033[31m"; 
		break;
		case "green": 
			$code = "\033[32m"; 
		break;
		case "reset": 
		default: 
		// reset the colour code to "normal"
			$code = "\033[0m";
	}
	echo $code;
}

/**
 * Check that the correct number of cli arguments have been supplied.
 * 
 * @param	Integer $argc The command line argument count.
 * @throws	Exception If the design pattern demonstration has not been specified.
 */
function checkArgs($argc) {
	if(!isset($argc) || $argc < 1) {
	// the actual pattern name is missing from the command
		throw new Exception("Please specify the pattern to demonstrate!");			
	}
}

try {
	checkArgs($argc);
	// try to get the demo
	$demo = Common\Demo\PatternDemoFactory::getDemo($argv[1]);
	$demo->run();
} catch (Exception $ex) {
// something went wrong with getting the pattern demo
	clic("red");
	echo $ex->getMessage() . "\n";
	clic("reset");
}


