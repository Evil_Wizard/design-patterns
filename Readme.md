# PHP Implemented Design Patterns
----

The design patterns used here were initially from a Java source.
Each design pattern comes with a demonstration.
Some patterns and types have been combined to create a more realistic implementation, whereas others have been left as a challenge.

## Creational Type Patterns
----

- [Factory](Creational/Factory/factory.md)
- [Abstract Factory](Creational/AbstractFactory/abstractfactory.md)
- [Singleton](Creational/Singleton/singleton.md)
- [Builder](Creational/Builder/builder.md)
- [Prototype](Creational/Prototype/prototype.md)

## Behavioral Type Patterns
----

## Structural Type Patterns
----
