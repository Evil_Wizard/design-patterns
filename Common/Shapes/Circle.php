<?php

namespace Common\Shapes;

use Common\Shape;

/**
 * Class Circle
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Shape As a base for the shape type.
 */
class Circle extends Shape {
	
	/**
	 * Circle constructor.
	 */
	public function __construct() {
		parent::__construct("Circle");
	}
	
	/**
	 * Draw the circle shape.
	 */
	public function draw() {
		// the specific logic to draw the shape
		echo "Drawing a circle\n";
	}
	
}
