<?php

namespace Common\Shapes;

use Common\Shape;

/**
 * Class Square
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	Shape As a base for the shape type.
 */
class Square extends Shape {
	
	/**
	 * Square constructor.
	 */
	public function __construct() {
		parent::__construct("Square");
	}
	
	/**
	 * Draw the square shape.
	 */
	public function draw() {
		// the specific logic to draw the shape
		echo "Drawing a square\n";
	}
	
}
