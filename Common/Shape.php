<?php

namespace Common;

/**
 * Abstract Class Shape
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 16-Mar-2019 Evil Wizard Creation.
 */
abstract class Shape {
	
	/*
	 * @var	String The shape ID.
	 */
	protected $id = NULL;
	
	/*
	 * @var	String The shape type.
	 */
	protected $type = NULL;

	/**
	 * Shape constructor.
	 * 
	 * @param	String $type The shape type.
	 */
	protected function __construct($type) {
		$this->type = $type;
		echo "New {$this->type} created\n";
	}
	
	/**
	 * Clone the Shape object.
	 */
	public function __clone() { 
		$this->type = $this->type;
		$this->id = $this->id;
	}
	
	/**
	 * Get the shape type.
	 *
	 * @return	String The shape type.
	 */
	public function getType() { return $this->type; }
	
	/**
	 * Get the shape ID.
	 *
	 * @return	String The shape ID.
	 */
	public function getId() { return $this->id; }
	
	/**
	 * Set the shape ID.
	 *
	 * @param	String $id The shape ID.
	 */
	public function setId($id) { $this->id = $id; }

}
