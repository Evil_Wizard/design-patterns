<?php

namespace Common;

/**
 * Class ShapeTypes
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
class ShapeTypes {
	
	/*
	 * @var	String Identify the shape CIRCLE.
	 */
	const CIRCLE = "CIRCLE";
	
	/*
	 * @var	String Identify the shape RECTANGLE.
	 */
	const RECTANGLE = "RECTANGLE";
	
	/*
	 * @var	String Identify the shape SQUARE.
	 */
	const SQUARE = "SQUARE";
	
}
