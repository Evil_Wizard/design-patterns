<?php

namespace Common\Demo;

/**
 * Class APatternDemo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
abstract class APatternDemo extends ADemo {
	
	/*
	 * @var	String The design type group the pattern falls into.
	 */
	protected $pattern_type = NULL;
	
	/*
	 * @var	String The design type pattern name.
	 */
	protected $pattern_name = NULL;

	/**
	 * APatternDemo constructor.
	 *
	 * @param	String $pattern_name The design type pattern name.
	 * @param	String $pattern_type The design type group the pattern falls into.
	 */
	protected function __construct($pattern_name, $pattern_type) {
		parent::__construct();
		// set the design pattern details
		$this->pattern_name = $pattern_name;
		$this->pattern_type = $pattern_type;
	}
	
}
