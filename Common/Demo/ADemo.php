<?php

namespace Common\Demo;

/**
 * Abstract Class ADemo
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 */
abstract class ADemo {

	/**
	 * ADemo constructor.
	 */
	protected function __construct() {
		;
	}
	
	/**
	 * Run the specific demonstraction.
	 */
	abstract public function run();

}
