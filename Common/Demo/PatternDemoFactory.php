<?php

namespace Common\Demo;

use ReflectionClass;
use ReflectionException;
use Exception;

/**
 * Class PatternDemoFactory
 * 
 * @version 1.0.0
 * @author Russell Nash <evil.wizard95@googlemail.com>
 * @copyright 2019 Evil Wizard Creation.
 * 
 * @uses	ReflectionClass To get the route controller.
 * @uses	ReflectionException Catches named exception.
 * @uses	Exception For error and exception handling.
 */
class PatternDemoFactory {
	
	/*
	 * @var	Array List of design patterns and the group type it belongs to.
	 */
	protected static $patterns = [
		"Factory"				=> "Creational",
		"AbstractFactory"		=> "Creational",
		"Singleton"				=> "Creational",
		"Builder"				=> "Creational",
		"Prototype"				=> "Creational"
	];
	
	/**
	 * Static function to get a specified design pattern demonstration.
	 * 
	 * @param	String $pattern The design pattern name.
	 * @return	APatternDemo The required design pattern demonstration.
	 * @throws	Exception If the design pattern demonstration is unknown.
	 */
	public static function getDemo($pattern) {
		// determine what factory shape product has been requested
		if(!array_key_exists($pattern, static::$patterns)) {
		// unknown design pattern demo requested
			throw new Exception("Unknown design pattern [{$pattern}]");			
		}
		// get the pattern type form the pattern name array as the pattern will 
		// be namespaced in the group type
		$pattern_type = static::$patterns[$pattern];
		$demo_class_ns = "\\{$pattern_type}\\{$pattern}\Demo";
		try {
			// try to get the demo
			$demo_reflection = new ReflectionClass($demo_class_ns);
		} catch (ReflectionException $ex) {
		// pattern demo didn't exist
			throw new Exception("Pattern demo [{$demo_class_ns}] can not be found", $ex->getCode(), $ex);
		}
		if(!$demo_reflection->isSubclassOf(APatternDemo::class)) {
		// pattern demo doesn't extend from the abstract pattern demo base
			throw new Exception("Pattern demo is not of a recognised heritage");							
		}
		// return the pattern demo
		return $demo_reflection->newInstance();
	}
	
}
